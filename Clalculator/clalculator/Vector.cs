﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clalculator
{
    class Vector
    {
        int x, y;

        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return "x: "+ X + " y: " + Y;
        }

        public Vector Add(Vector v)
        {
            Vector rt =  new Vector(X+v.X, Y+v.Y);
            return rt;
        }


        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
    }


}
